# Agroweather - WebApp

Agroweather WebApp is an app written in Javascript and Vue that represents frontend part of the Agroweather app.

## Installation

Make sure you're running Node >= 12.

Use the npm package manager or yarn to install packages.

```bash
yarn install
```

## Usage

```bash
# Run build
yarn build

# Run development
yarn serve

# Run production (using http-server)
yarn production
# by default runs the app on port 80 and enables cache with 1 hour interval
```

## Hints
### Running in production
The app doesn't need to be run in production, it's enough to serve the ```dist``` folder with ```nginx``` or ```apache``` after building it. 

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

## License
[MIT](https://choosealicense.com/licenses/mit/)