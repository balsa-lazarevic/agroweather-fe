export const navigationRoutes = {
  root: {
    name: '/',
    displayName: 'navigationRoutes.home',
  },
  routes: [
    {
      name: 'trenutno-vrijeme',
      displayName: 'menu.trenutno-vrijeme',
      meta: {
        iconClass: 'entypo entypo-light-up',
      },
    },
    {
      name: 'vremenska-prognoze',
      displayName: 'menu.vremenska-prognoza',
      meta: {
        iconClass: 'entypo entypo-fast-forward',
      },
    },
    {
      name: 'obavjestenja',
      displayName: 'menu.obavjestenja',
      meta: {
        iconClass: 'entypo entypo-chat',
      },
    },
    {
      name: 'lokacije',
      displayName: 'menu.lokacije',
      meta: {
        iconClass: 'entypo entypo-location',
      },
    },
    {
      name: 'o-projektu',
      displayName: 'menu.o-projektu',
      meta: {
        iconClass: 'entypo entypo-book',
      },
    },
  ],
}
