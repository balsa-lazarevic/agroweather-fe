import Vue from 'vue'
import Router from 'vue-router'
import AuthLayout from '../components/auth/AuthLayout'
import AppLayout from '../components/admin/AppLayout'

Vue.use(Router)

const EmptyParentComponent = {
  template: '<router-view></router-view>',
}

let router = new Router({
  mode: process.env.VUE_APP_ROUTER_MODE_HISTORY === 'true' ? 'history' : 'hash',
  routes: [
    {
      path: '/auth',
      component: AuthLayout,
      children: [
        {
          name: 'prijava',
          path: 'prijava',
          component: () => import('../components/auth/login/Login.vue'),
        },
        {
          name: 'registracija',
          path: 'registracija',
          component: () => import('../components/auth/signup/Signup.vue'),
        },
        {
          name: 'promo',
          path: 'promo',
          component: () => import('../components/auth/promo/Promo.vue'),
        },
      ],
    },
    {
      name: 'Aplikacija',
      path: '/',
      component: AppLayout,
      children: [
        {
          name: 'trenutno-vrijeme',
          path: 'trenutno-vrijeme',
          alias: "/",
          component: () => import('../components/vrijeme/Trenutno.vue'),
          default: true,
          meta: {
            requiresAuth: true,
          }
        },
        {
          name: 'vremenska-prognoze',
          path: 'vremenska-prognoze',
          component: () => import('../components/vrijeme/Prognoze.vue'),
          meta: {
            requiresAuth: true,
          }
        },
        {
          path: 'obavjestenja',
          component: () => import('../components/obavjestenja/ObavjestenjaView.vue'),
          meta: {
            requiresAuth: true,
          },
          children: [
            {
              name: 'obavjestenja',
              path: '',
              component: () => import('../components/obavjestenja/Obavjestenja.vue'),
              default: true,
              meta: {
                requiresAuth: true,
              }
            },
            {
              name: 'novo',
              path: 'novo',
              component: () => import('../components/obavjestenja/NovoObavjestenje.vue'),
              meta: {
                requiresAuth: true,
              }
            },
            ,
            {
              name: 'izmijeni',
              path: 'izmijeni/:id',
              component: () => import('../components/obavjestenja/IzmijeniObavjestenje.vue'),
              meta: {
                requiresAuth: true,
              }
            },
          ]
        },
        {
          path: 'lokacije',
          component: () => import('../components/lokacije/LokacijeView.vue'),
          meta: {
            requiresAuth: true,
          },
          children: [
            {
              name: 'lokacije',
              path: '',
              component: () => import('../components/lokacije/Lokacije.vue'),
              default: true,
              meta: {
                requiresAuth: true,
              }
            },
            {
              name: 'nova',
              path: 'nova',
              component: () => import('../components/lokacije/NovaLokacija.vue'),
              meta: {
                requiresAuth: true,
              }
            },
            ,
            {
              name: 'izmijeni',
              path: 'izmijeni/:id',
              component: () => import('../components/lokacije/IzmijeniLokaciju.vue'),
              meta: {
                requiresAuth: true,
              }
            },
          ]
        },
        {
          name: 'o-projektu',
          path: 'o-projektu',
          component: () => import('../components/o-projektu/Oprojektu.vue'),
          default: true,
          meta: {
            requiresAuth: true,
          }
        },
        {
          name: 'profil',
          path: 'profil',
          component: () => import('../components/profil/Profil.vue'),
          meta: {
            requiresAuth: true,
          }
        },
        {
          name: '404',
          path: '*',
          component: () => import('../components/strane/404.vue'),
          meta: {
            requiresAuth: true,
          }
        },
      ],
      meta: {
        requiresAuth: true,
      }
    }
  ],
})

router.beforeEach((to, from, next) => {
  let JWT = localStorage.getItem('jwt');
  if(to.matched.some(record => record.meta.requiresAuth)) {
    if (JWT == null || JWT == 'undefined') {
      next({
          path: '/auth/prijava',
          params: { nextUrl: to.fullPath }
      })
    }
    else {
      next() 
    }
  } else {
    if(to.path === '/'){
      next({
        path: '/trenutno-vrijeme',
      })
      return
    }
    next() 
  }
})

export default router